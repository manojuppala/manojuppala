<p align="center">
<img align='center' src='https://user-images.githubusercontent.com/5713670/87202985-820dcb80-c2b6-11ea-9f56-7ec461c497c3.gif' width='200"'>
</p>

<p align='center'>
<a href="https://gitlab.com/manojuppala">
<img src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white"/>
</a>
<a href="https://stackoverflow.com/users/11874811/manoj">
<img src="https://img.shields.io/badge/Stack_Overflow-FE7A16?style=for-the-badge&logo=stack-overflow&logoColor=white"/>
</a>
<a href="https://manojuppala.com">
<img src="https://img.shields.io/badge/website-000000?style=for-the-badge&logo=About.me&logoColor=white"/>
</a>
<a href="https://www.linkedin.com/in/manojuppala/">
<img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"/>
</a>
<a href="https://www.youtube.com/channel/UCX93oEN0tza6KfuAWfI61vQ">
<img src="https://img.shields.io/badge/YouTube-FF0000?style=for-the-badge&logo=youtube&logoColor=white"/>
</a>
</p>

### 📌Pinned Repositories

<p align="left">
<a href="https://github.com/manojuppala/camKapture">
  <img width='350em' src="https://github-readme-stats.vercel.app/api/pin/?username=manojuppala&repo=camKapture&title_color=ffffff&icon_color=ffffff&text_color=ffffff&bg_color=343a40" />
</a>
<a href="https://github.com/manojuppala/dotfiles">
  <img width='350em' src="https://github-readme-stats.vercel.app/api/pin/?username=manojuppala&repo=dotfiles&title_color=ffffff&icon_color=ffffff&text_color=ffffff&bg_color=343a40" />
</a>
<a href="https://github.com/manojuppala/tasklist-cli">
  <img width='350em' src="https://github-readme-stats.vercel.app/api/pin/?username=manojuppala&repo=tasklist-cli&title_color=ffffff&icon_color=ffffff&text_color=ffffff&bg_color=343a40" />
</a>
<a href="https://github.com/manojuppala/sorting-algorithm-visualizer">
  <img width='350em' src="https://github-readme-stats.vercel.app/api/pin/?username=manojuppala&repo=sorting-algorithm-visualizer&title_color=ffffff&icon_color=ffffff&text_color=ffffff&bg_color=343a40" />
</a>
</p>

---

### 🛠 Languages and Tools

<!-- took all these icons from
https://github.com/alexandresanlim/Badges4-README.md-Profile -->
<div>
    <img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white" />
    <img src="https://img.shields.io/badge/Flask-000000?style=for-the-badge&logo=flask&logoColor=white" />
    <img src="https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black" />
    <img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white" />
    <img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" />
    <img src="https://img.shields.io/badge/Material%20UI-007FFF?style=for-the-badge&logo=mui&logoColor=white" />
    <img src="https://img.shields.io/badge/C-00599C?style=for-the-badge&logo=c&logoColor=white" />
    <img src="https://img.shields.io/badge/Shell_Script-121011?style=for-the-badge&logo=gnu-bash&logoColor=white">
    <img src="https://img.shields.io/badge/Git-F05032?style=for-the-badge&logo=git&logoColor=white" />
    <img src="https://img.shields.io/badge/VIM-%2311AB00.svg?&style=for-the-badge&logo=vim&logoColor=white">
    <img src="https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white" />
    <img src="https://img.shields.io/badge/Linux_Mint-87CF3E?style=for-the-badge&logo=linux-mint&logoColor=white" />
 
</div>

---

### GitHub stats

<p align="left">
<a href="https://github.com/manojuppala">
  <img height="160em" src="https://github-readme-stats-git-master.manojuppala.vercel.app/api?username=manojuppala&&show_icons=true&title_color=ffffff&icon_color=ffffff&text_color=ffffff&bg_color=343a40" />
  <img height="160em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=manojuppala&layout=compact&title_color=ffffff&icon_color=ffffff&text_color=ffffff&bg_color=343a40" />

</a>
</p>
